Object.assign(game.realms.gameWorld.guiTypes.console.commands, {
	whoIsHere: {
		help: "whoIsHere",
		
		run: function(parameters, object) {
			object.log += Object.keys(
				app.virtualClient.players.list
			).join(", ") + "\n";
		}
	},
	
	runCommandAs: {
		help: "runCommandAs {player: [String], command: [String], parameters: [Object]}",
		
		run: function(parameters, object) {
			if (!parameters) {
				throw new SyntaxError("no parameters");
			}
			
			if (!(parameters.player in app.virtualClient.players.list)) {
				throw new ReferenceError("no such player");
			}
			
			var
				currentPlayer = app.virtualClient.players.current,
				
				virtualConsole = new game.realms.gameWorld.objectTypes.console.New();
			
			virtualConsole.log = "";
			
			app.virtualClient.players.setCurrent(parameters.player);
			
			try {
				(
					game.realms.gameWorld.guiTypes.console.commands[
						parameters.command
					] ||
					game.realms.gameWorld.guiTypes.console.commands.noSuchCommand
				).run(parameters.parameters, virtualConsole);
			} catch (error) {
				virtualConsole.log += error + "\n";
			}
			
			virtualConsole.log +=
				"---\n" +
				"[" + app.virtualClient.players.current + "] " +
				parameters.command + " " +
				JSON.stringify(parameters.parameters);
			
			alert(virtualConsole.log);
			
			app.virtualClient.players.setCurrent(currentPlayer);
			
			object.log += "Done\n";
		}
	}
});
