game.input.keyboard = {
	//Secondary variables
		pressed: [],
		released: [],
		
		mode: 0,
	
	//Methods
		catch: function(keyCode, action) {
			switch (this.mode) {
				case 0:
					({
						press: this.pressed,
						release: this.released
					})[action].push(keyCode);
					
					break;
			}
		},
		
		flush: function() {
			this.pressed.length = 0;
			this.released.length = 0;
		}
};
