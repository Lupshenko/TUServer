## Different (see LICENSE for each folder inside)

* node_modules/

## BSD Zero Clause License (LICENSE.0BSD)

* client/BigInteger.js ([BigInteger](https://www.npmjs.com/package/big-integer))
* All other files
